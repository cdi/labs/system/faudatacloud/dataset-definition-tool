"""
What we need:
- Checksum, File Size, File name
- check the file names -> ask the user to change them
- as single select and multi-select (no folders)
- write as one json? csv? idk yet (but CRIS can do python, so anything really?)
"""

import os
import tkinter as tk
from tkinter import filedialog
import re


def check_file_name(file):
    matches = re.findall(r'[\w,\d,_,-,.]+', file)

    print('looks ok',  matches)

    if len(matches) == 1:
        # all is ok
        return True
    else:
        f = file
        for m in matches:
            f = f.replace( m, '')
        print('no: forbidden chars:', f)
        return False



def get_file_size(file_path):
    """
    :return: file size in bytes
    """
    b = os.path.getsize(file_path)
    print(b)
    return b


def get_sha256_checksum(file_path):
    # Python program to find SHA256 hash string of a file
    import hashlib

    sha256_hash = hashlib.sha256()
    with open(file_path, "rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
        print(sha256_hash.hexdigest())
        return sha256_hash.hexdigest()


if __name__ == '__main__':
    # os specific file selector, get the full path and filename
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename()
    file = os.path.basename(file_path)

    print(file_path)
    if check_file_name(file):
        get_file_size(file_path)
        get_sha256_checksum(file_path)
